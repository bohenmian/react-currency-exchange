import React, {Component} from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {
  render() {
    return (
      <div>
        <CurrencyInput currency="USD"/>
        <CurrencyInput currency="CNY"/>
      </div>
    )
  }

}

export default CurrencyTranslator;